#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""Test for separate network and mount namespaces."""

import os
import sys
import unittest


class TestIsolation(unittest.TestCase):
    """Test for separate network and mount namespaces.

    Test that Rhosydd and its mock backends are each running in a separate
    network namespace, which are all separate from the default (which we assume
    this test is running in). Similarly, test that they are running in separate
    mount namespaces, and hence have some kind of bind mounts giving them a
    different view of the file system.

    Note that this test does not check //how// that view of the file system
    differs.

    This test must be run as root in order to be able to read arbitrary cmdline
    files from /proc.

    See:
     • https://www.freedesktop.org/software/systemd/man/systemd.exec.html
       - PrivateNetwork
       - PrivateDevices
       - PrivateTmp
       - ProtectHome
       - ProtectSystem
     • https://lwn.net/Articles/531381/
    """

    @classmethod
    def setUpClass(cls):
        # Dictionary mapping namespace type to a list of namespaces of that
        # type for all the Rhosydd processes we found.
        cls.rhosydd_namespaces = {
            'net': [],
            'mnt': [],
        }

        for pid in os.listdir('/proc'):
            try:
                with open('/proc/{}/cmdline'.format(pid), mode='rb') as fd:
                    cmdline = fd.read().decode().split('\x00')
            except Exception:
                continue

            if 'rhosydd' not in cmdline[0] or cmdline[0] == 'rhosydd-client':
                continue

            print('Got namespaces for {}:'.format(cmdline[0]))
            for namespace_type in cls.rhosydd_namespaces.keys():
                namespace = \
                    TestIsolation.__get_process_namespace(pid, namespace_type)
                cls.rhosydd_namespaces[namespace_type].append(namespace)
                print(' - {}: {}'.format(namespace_type, namespace))

        cls.test_namespaces = {}
        cls.pid1_namespaces = {}

        for namespace_type in cls.rhosydd_namespaces.keys():
            cls.test_namespaces[namespace_type] = \
                TestIsolation.__get_process_namespace('self', namespace_type)
            cls.pid1_namespaces[namespace_type] = \
                TestIsolation.__get_process_namespace('1', namespace_type)

        # Output the namespaces for debugging.
        print('Found Rhosydd namespaces:')
        for namespace_type, namespaces in cls.rhosydd_namespaces.items():
            for namespace in namespaces:
                print(' - {}: {}'.format(namespace_type, namespace))

        print('Got namespaces for test process:')
        for namespace_type, namespace in cls.test_namespaces.items():
            print(' - {}: {}'.format(namespace_type, namespace))

        print('Got namespaces for PID1:')
        for namespace_type, namespace in cls.pid1_namespaces.items():
            print(' - {}: {}'.format(namespace_type, namespace))

    @classmethod
    def tearDownClass(cls):
        cls.rhosydd_namespaces = None
        cls.test_namespaces = None
        cls.pid1_namespaces = None

    def setUp(self):
        # Sanity check that we actually have some namespaces.
        if len(self.rhosydd_namespaces['net']) == 0:
            self.fail('Couldn’t find any Rhosydd processes')

    @staticmethod
    def __get_process_namespace(pid, namespace_type):
        """Get the namespace for the given process and type.

        Type might be ‘net’ or ‘mnt’, for example. These files in /proc are
        dangling symlinks whose targets give equivalence classes of namespaces.
        A ‘net’ namespace might have a target of ‘net:[4026532179]’ for
        example.
        """
        return os.readlink('/proc/{}/ns/{}'.format(pid, namespace_type))

    def test_test_and_pid1_namespaces(self):
        """Sanity check the namespaces for this process and PID1 are equal.

        Otherwise something might have changed in the /proc tree structure
        which might give us false test passes).
        """
        for namespace_type, namespace in self.test_namespaces.items():
            if namespace != self.pid1_namespaces[namespace_type]:
                self.fail('Test process and PID1 {} namespaces were distinct '
                          'when they are not expected to '
                          'be'.format(namespace_type))

    def test_namespaces_unique(self):
        """Check all the Rhosydd namespaces are unique."""
        for namespace_type, namespaces in self.rhosydd_namespaces.items():
            if len(namespaces) != len(set(namespaces)):
                self.fail('Some {} namespaces were not '
                          'unique'.format(namespace_type))

    def test_namespaces_disjoint(self):
        """Check all the Rhosydd namespaces diff from the test namespace."""
        for namespace_type, namespace in self.test_namespaces.items():
            if namespace in self.rhosydd_namespaces[namespace_type]:
                self.fail('{} namespaces were not distinct from others on the '
                          'system'.format(namespace_type))


if __name__ == '__main__':
    unittest.main()
