/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <json-c/json.h>

#include "vehicle_builder.h"
#include "librhosydd/types.h"
#include "librhosydd/attribute.h"

void          parse_json_object        (json_object *object);
GVariant      *build_metadata          (json_object *object);
GVariant      *get_default_value       (char *data_type);

static GPtrArray /*RsdAttributeInfo*/  *vehicle_attributes = NULL;

GPtrArray /*RsdAttributeInfo*/ *build_vehicle (void)
{
  json_object *root = json_object_from_file (SPEEDOBACKENDDIR"/speedo_attributes.json");

  if (json_object_get_type(root) != json_type_object)
    {
         return NULL;
    }

  vehicle_attributes = g_ptr_array_new_with_free_func ((GDestroyNotify)
                                                       rsd_attribute_info_free);

  parse_json_object(root);
  return vehicle_attributes;
}


void parse_json_object (json_object *object)
{
  static char *signal_name = NULL;

  json_object_object_foreach (object, key, sub_object)
    {
       char *dot;
       json_object *value;
       char *type; 
       if (signal_name == NULL)
         {
            signal_name = (char*) malloc(strlen(key)+1);
            strcpy(signal_name, key);
         }
       else
         {
            signal_name = (char*) realloc (signal_name,
                                           strlen(signal_name) + 
                                           strlen(key) + 2);
            strcat (signal_name, ".");
            strcat (signal_name, key);
         }

       if (!json_object_object_get_ex (sub_object, "type", &value))
          continue;
 
       if (json_object_get_type (value) != json_type_string)
          continue;
           
       type = (char *) json_object_get_string (value);

       if (strcmp (json_object_get_string (value), "branch") == 0)
         {
            if (!json_object_object_get_ex (sub_object, "children", &value))
               continue;
            parse_json_object (value);
         }    
       else
         {
            GVariant* default_value = get_default_value (type);
            GVariant* metainfo = build_metadata (sub_object);

            if ( default_value && metainfo )
              {
                 RsdAttribute *attribute = rsd_attribute_new ( default_value,
                                                               0.0,
                                                               g_get_monotonic_time());
                 RsdAttributeMetadata *metadata = rsd_attribute_metadata_new (
                                                  signal_name, RSD_ATTRIBUTE_AVAILABLE,
                                                  RSD_ATTRIBUTE_READABLE, metainfo);

                 g_ptr_array_add (vehicle_attributes,
                                  rsd_attribute_info_new (attribute, metadata));

                 rsd_attribute_free (attribute);
                 rsd_attribute_metadata_free (metadata);
              } 
           }
       dot = strrchr (signal_name, '.');
       if (dot) *dot = '\0';
       else
         {
            free (signal_name);
            signal_name = NULL;
         }
    }
}

GVariant *build_metadata (json_object *object)
{
  GVariantBuilder builder;
  GVariant *l_variant = NULL;
 
  g_variant_builder_init (&builder, G_VARIANT_TYPE("a{sv}"));
  json_object_object_foreach (object, key, val)
    {
       switch (json_object_get_type (val))
         {
            case json_type_string:
                 l_variant = g_variant_ref_sink (g_variant_new_string (
                                                json_object_get_string(val)));
            break;

            case json_type_boolean:
                 l_variant = g_variant_ref_sink (g_variant_new_boolean (
                                                 json_object_get_boolean(val)));
            break;

            case json_type_double:
                 l_variant = g_variant_ref_sink (g_variant_new_double (
                                                 json_object_get_double(val)));
                 
            break;

            case json_type_int:
                 l_variant = g_variant_ref_sink (g_variant_new_uint32 (
                                                 json_object_get_int(val)));
            break;

            case json_type_null:
            case json_type_array:
            case json_type_object:
            default:
                 return NULL;
        }
       g_variant_builder_add (&builder, "{sv}", key, l_variant);
       g_variant_unref(l_variant);
    }
  return g_variant_ref_sink (g_variant_builder_end (&builder));
}

GVariant *get_default_value (char* data_type)
{
  if (!data_type || !strlen (data_type))
     return NULL;
  if (!strcmp (data_type, "UInt8"))
     return g_variant_ref_sink (g_variant_new_byte (0));
  if (!strcmp (data_type, "Int8"))
     return g_variant_ref_sink (g_variant_new_byte (0));
  if (!strcmp(data_type, "UInt16"))
     return g_variant_ref_sink (g_variant_new_uint16 (0));
  if (!strcmp (data_type, "Int16"))
     return g_variant_ref_sink (g_variant_new_int16 (0));
  if (!strcmp (data_type, "Int32"))
     return g_variant_ref_sink (g_variant_new_int32 (0));
  if (!strcmp (data_type, "UInt32"))
     return g_variant_ref_sink (g_variant_new_uint32 (0));
  if (!strcmp (data_type, "Int64"))
     return g_variant_ref_sink (g_variant_new_int64 (0));
  if (!strcmp (data_type, "UInt64"))
     return g_variant_ref_sink (g_variant_new_uint64 (0));
  if (!strcmp(data_type, "Boolean"))
     return g_variant_ref_sink (g_variant_new_boolean (0));
  if (!strcmp (data_type, "Float") || !strcmp (data_type, "Double"))
     return g_variant_ref_sink (g_variant_new_double (0.0));
  if (!strcmp (data_type, "String"))
     return g_variant_ref_sink (g_variant_new_string ("default"));
  return NULL;
}

void destroy_vehicle (void)
{
  g_clear_pointer (&vehicle_attributes, g_ptr_array_unref);
}
