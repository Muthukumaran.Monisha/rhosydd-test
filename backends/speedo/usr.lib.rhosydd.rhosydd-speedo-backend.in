# vim:syntax=apparmor
# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <tunables/global>

# (attach_disconnected) is needed to resolve paths from within the mount
# namespace caused by the ProtectSystem (and other) .service keys.
@backendbindir@/rhosydd-speedo-backend (attach_disconnected) {
  #include <abstractions/chaiwala-base>
  #include <abstractions/dbus-strict>

  # Well-known name
  dbus send
        bus=system
        path=/org/freedesktop/DBus
        interface=org.freedesktop.DBus
        member={RequestName,ReleaseName}
        peer=(name=org.freedesktop.DBus),
  dbus bind bus=system name="org.apertis.Rhosydd1.Backends.Speedo",

  @backendbindir@/rhosydd-speedo-backend mr,

  # Getting credentials from the daemon for authorisation checks on its AppArmor label
  dbus send
       bus=system
       path=/org/freedesktop/DBus
       interface=org.freedesktop.DBus
       member="GetConnectionCredentials"
       peer=(name=org.freedesktop.DBus),

  # systemd startup notification
  /run/systemd/notify w,

  # Talking with the daemon
  dbus (send, receive) bus=system peer=(label=/usr/bin/rhosydd),
}
