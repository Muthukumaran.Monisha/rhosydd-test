/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef SPEEDO_VEHICLE_H
#define SPEEDO_VEHICLE_H

#include <glib.h>
#include <glib-object.h>

#include "librhosydd/types.h"

G_BEGIN_DECLS

#define SPEEDO_TYPE_VEHICLE speedo_vehicle_get_type ()
G_DECLARE_FINAL_TYPE (SpeedoVehicle, speedo_vehicle, SPEEDO, VEHICLE, GObject)

SpeedoVehicle *speedo_vehicle_new    (void);
void           speedo_vehicle_update (SpeedoVehicle *self,
                                      RsdTimestampMicroseconds   timestamp);

G_END_DECLS

#endif /* !SPEEDO_VEHICLE_H */
