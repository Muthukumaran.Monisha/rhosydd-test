/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib-unix.h>
#include <glib/gi18n.h>
#include <gio/gio.h>
#include <locale.h>
#include <signal.h>

#include "libcroesor/backend.h"
#include "libcroesor/vehicle-manager.h"
#include "vehicle.h"


/* A way of automatically removing sources when going out of scope. */
typedef guint SourceId;
G_DEFINE_AUTO_CLEANUP_FREE_FUNC (SourceId, g_source_remove, 0)

static gboolean
update_cb (gpointer user_data)
{
  SpeedoVehicle *vehicle = SPEEDO_VEHICLE (user_data);

  speedo_vehicle_update (vehicle, g_get_real_time ());

  return G_SOURCE_CONTINUE;
}

int
main (int   argc,
      char *argv[])
{
  g_autoptr (GError) error = NULL;
  CsrVehicleManager *vehicle_manager;
  g_autoptr (GPtrArray/*<unowned RsdVehicle>*/) added = NULL;
  g_autoptr (CsrBackend) backend = NULL;
  g_autoptr (SpeedoVehicle) vehicle = NULL;
  g_auto (SourceId) timeout_id = 0;

  /* Localisation */
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  /* Create the backend. */
  backend = csr_backend_new ("org.apertis.Rhosydd1.Backends.Speedo",
                             "/org/apertis/Rhosydd1/Backends/Speedo",
                             GETTEXT_PACKAGE,
                             _("Export a mock vehicle object for use by the "
                               "vehicle device daemon."));
  vehicle_manager = csr_backend_get_vehicle_manager (backend);

  /* Add an example vehicle. */
  vehicle = speedo_vehicle_new ();

  added = g_ptr_array_new ();
  g_ptr_array_add (added, vehicle);
  csr_vehicle_manager_update_vehicles (vehicle_manager, added, NULL);

  /* Add a timer to update the simulation. */
  timeout_id = g_timeout_add_seconds (1, update_cb, vehicle);

  /* Run until we are killed. */
  csr_service_run (CSR_SERVICE (backend), argc, argv, &error);

  if (error != NULL)
    {
      int code;

      if (g_error_matches (error,
                           CSR_SERVICE_ERROR, CSR_SERVICE_ERROR_SIGNALLED))
        raise (csr_service_get_exit_signal (CSR_SERVICE (backend)));

      g_printerr ("%s: %s\n", argv[0], error->message);
      code = error->code;

      return code;
    }

  return 0;
}
