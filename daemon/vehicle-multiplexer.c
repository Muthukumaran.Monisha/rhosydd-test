/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <gio/gio.h>

#include "libcroesor/vehicle-manager.h"
#include "libinternal/logging.h"
#include "librhosydd/vehicle.h"

#include "aggregate-vehicle.h"
#include "vehicle-multiplexer.h"


/**
 * vdd_vehicle_multiplexer_update_vehicles:
 * @added: (element-type RsdVehicle) (transfer none) (nullable): potentially
 *    empty array of newly added vehicles, or %NULL to represent the empty array
 * @removed: (element-type RsdVehicle) (transfer none) (nullable): potentially
 *    empty array of newly removed vehicles, or %NULL to represent the empty
 *    array
 *
 * Update the #VddAggregateVehicles in the given #CsrVehicleManager, adding and
 * removing #RsdProxyVehicle instances given in @added and @removed. This may
 * involve destroying #VddAggregateVehicles where all of their proxy vehicles
 * have been removed — or creating new #VddAggregateVehicles for previously
 * unseen proxy vehicle IDs.
 *
 * Each #RsdProxyVehicle in @added and @removed is matched to a single
 * #VddAggregateVehicle by its ID; each #VddAggregateVehicle contains only
 * proxy vehicles of the same ID.
 *
 * Since: 0.3.0
 */
void
vdd_vehicle_multiplexer_update_vehicles (GPtrArray             *added,
                                         GPtrArray             *removed,
                                         CsrVehicleManager     *output_manager)
{
  g_autoptr (GPtrArray/*<unowned RsdVehicle>*/) added_aggregates = NULL;
  g_autoptr (GPtrArray/*<unowned RsdVehicle>*/) removed_aggregates = NULL;
  gsize i;

  g_return_if_fail (CSR_IS_VEHICLE_MANAGER (output_manager));

  added_aggregates = g_ptr_array_new_with_free_func (NULL);
  removed_aggregates = g_ptr_array_new_with_free_func (NULL);

  /* Removed vehicles. */
  for (i = 0; removed != NULL && i < removed->len; i++)
    {
      VddAggregateVehicle *aggregate;
      RsdVehicle *vehicle = RSD_VEHICLE (removed->pdata[i]);
      const gchar *vehicle_id;
      GPtrArray/*<owned RsdVehicle>*/ *aggregate_vehicles;

      vehicle_id = rsd_vehicle_get_id (RSD_VEHICLE (vehicle));
      aggregate = VDD_AGGREGATE_VEHICLE (csr_vehicle_manager_get_vehicle (output_manager,
                                                                          vehicle_id));

      DEBUG ("Removing vehicle %p (‘%s’) from aggregate %p.",
             vehicle, vehicle_id, aggregate);

      if (aggregate == NULL)
        {
          WARNING ("Proxy vehicle ‘%s’ removed without being in an aggregate.",
                   vehicle_id);
          continue;
        }

      vdd_aggregate_vehicle_update_vehicles (aggregate, NULL,
                                             RSD_VEHICLE (vehicle));

      aggregate_vehicles = vdd_aggregate_vehicle_get_vehicles (aggregate);

      if (aggregate_vehicles->len == 0)
        g_ptr_array_add (removed_aggregates, aggregate);
    }

  /* Added vehicles. */
  for (i = 0; added != NULL && i < added->len; i++)
    {
      VddAggregateVehicle *aggregate;
      RsdVehicle *vehicle = RSD_VEHICLE (added->pdata[i]);
      const gchar *vehicle_id;
      gsize j;

      vehicle_id = rsd_vehicle_get_id (RSD_VEHICLE (vehicle));
      aggregate = VDD_AGGREGATE_VEHICLE (csr_vehicle_manager_get_vehicle (output_manager,
                                                                          vehicle_id));

      DEBUG ("Adding vehicle %p (‘%s’) to aggregate %p.",
             vehicle, vehicle_id, aggregate);

      /* See if it was just removed above. */
      for (j = 0; aggregate == NULL && j < removed_aggregates->len; j++)
        {
          VddAggregateVehicle *removed_aggregate = removed_aggregates->pdata[j];

          if (g_strcmp0 (vehicle_id,
                         rsd_vehicle_get_id (RSD_VEHICLE (removed_aggregate))) == 0)
            {
              aggregate = removed_aggregate;
              g_ptr_array_remove_index_fast (removed_aggregates, j);
              break;
            }
        }

      if (aggregate == NULL)
        {
          aggregate = vdd_aggregate_vehicle_new (vehicle_id);
          g_ptr_array_add (added_aggregates, aggregate);
        }

      vdd_aggregate_vehicle_update_vehicles (aggregate, RSD_VEHICLE (vehicle),
                                             NULL);
    }

  /* Update the aggregated vehicles in the vehicle manager. */
  if (added_aggregates->len > 0 || removed_aggregates->len > 0)
    {
      DEBUG ("Updating aggregate vehicles with %u added and %u removed.",
             added_aggregates->len, removed_aggregates->len);
      csr_vehicle_manager_update_vehicles (output_manager,
                                           added_aggregates,
                                           removed_aggregates);
    }
}
