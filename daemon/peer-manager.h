/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef VDD_PEER_MANAGER_H
#define VDD_PEER_MANAGER_H

#include <glib.h>
#include <glib-object.h>

#include <libcroesor/peer-manager.h>

G_BEGIN_DECLS

#define VDD_TYPE_PEER_MANAGER vdd_peer_manager_get_type ()
G_DECLARE_FINAL_TYPE (VddPeerManager, vdd_peer_manager, VDD,
                      PEER_MANAGER, GObject)

void            vdd_peer_manager_new_async  (GCancellable         *cancellable,
                                             GAsyncReadyCallback   callback,
                                             gpointer              user_data);
VddPeerManager *vdd_peer_manager_new_finish (GAsyncResult         *result,
                                             GError              **error);

G_END_DECLS

#endif /* !VDD_PEER_MANAGER_H */
