Source: rhosydd
Section: libs
Priority: extra
Maintainer: Apertis maintainers <maintainers@lists.apertis.org>
Build-Depends:
 autoconf-archive (>= 20150925-1co1),
 autotools-dev,
 debhelper (>= 10),
 dh-apparmor,
 gobject-introspection,
 libgirepository1.0-dev,
 libglib2.0-dev (>= 2.58.2),
 libpolkit-gobject-1-dev,
 libsystemd-dev,
 libjson-c-dev,
 libyaml-dev,
Standards-Version: 3.9.6

Package: gir1.2-croesor-0
Section: introspection
Architecture: any
Multi-arch: same
Depends:
 ${gir:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Description: Sensors and actuators API - introspection bindings for backends
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains a machine-readable API description for the shared
 library for use by sensor and actuator backends. It is not part of the
 SDK-API and should not be used by applications.

Package: gir1.2-rhosydd-0
Section: introspection
Architecture: any
Multi-arch: same
Depends:
 ${gir:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Description: Sensors and actuators API - GObject-Introspection bindings
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains a machine-readable API description for the shared
 library for use by general client applications. It is part of the SDK-API.

Package: libcroesor-0-4
Architecture: any
Multi-arch: same
Priority: optional
Section: oldlibs
Depends:
 libcroesor-0-5,
 ${misc:Depends},
 ${shlibs:Depends},
Description:  This is a transitional package. It can safely be removed.
 .
 This is a transitional package for libcroesor.

Package: libcroesor-0-5
Architecture: any
Multi-arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Replaces: libcroesor-0-4
Breaks: libcroesor-0-4
Description: Sensors and actuators API - shared library for backends
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains the shared library for use by sensor and
 actuator backends. It is not part of the SDK-API and should not be
 used by applications.

Package: librhosydd-0-4
Architecture: any
Multi-arch: same
Priority: optional
Section: oldlibs
Depends:
 librhosydd-0-6,
 ${misc:Depends},
 ${shlibs:Depends},
Description:  This is a transitional package. It can safely be removed.
 .
 This is a transitional package for librhosydd.

Package: librhosydd-0-6
Architecture: any
Multi-arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Replaces: librhosydd-0-4
Breaks: librhosydd-0-4
Description: Sensors and actuators API - shared library
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains the shared library for use by general
 client applications. It is part of the SDK-API.

Package: libcroesor-0-dev
Section: libdevel
Architecture: any
Multi-arch: same
Depends:
 gir1.2-croesor-0 (= ${binary:Version}),
 libcanterbury-0-dev (>= 0.10),
 libcroesor-0-5 (= ${binary:Version}),
 libglib2.0-dev,
 libpolkit-gobject-1-dev,
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 libcroesor-0-doc,
Description: Sensors and actuators API - development library for backends
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains development files for use by sensor and
 actuator backends. It is not part of the SDK-API and should not be
 used by applications.

Package: libcroesor-0-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Sensors and actuators API - documentation for backends
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains reference documentation for the APIs used
 by sensor and actuator backends.

Package: libcroesor-0-tests
Section: misc
Architecture: any
Depends:
 libcroesor-0-5 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Sensors and actuators API - tests
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains unit tests for the APIs used by sensor and
 actuator backends.

Package: librhosydd-0-dev
Section: libdevel
Architecture: any
Multi-arch: same
Depends:
 gir1.2-rhosydd-0 (= ${binary:Version}),
 librhosydd-0-6 (= ${binary:Version}),
 libglib2.0-dev,
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 librhosydd-0-doc,
Description: Sensors and actuators API - development library
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains the development files for use by general
 client applications. It is part of the SDK-API.

Package: librhosydd-0-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Sensors and actuators API - documentation
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains reference documentation for the APIs used
 by general client applications.

Package: librhosydd-0-tests
Section: misc
Architecture: any
Depends:
 librhosydd-0-6 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Sensors and actuators API - tests
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains unit tests for the APIs used by general
 client applications.

Package: rhosydd
Section: misc
Architecture: any
Depends:
 acl,
 adduser,
 policykit-1 (>= 0.113-1co2~),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Sensors and actuators service
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains the service itself.

Package: rhosydd-mock-backends
Section: misc
Architecture: any
Depends:
 rhosydd,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Sensors and actuators service - mock backends
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains mock backends for integration testing only.

Package: rhosydd-tests
Section: misc
Architecture: any
Depends:
 gir1.2-rhosydd-0,
 python3-gi,
 rhosydd,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Sensors and actuators service - tests
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains integration tests.

Package: rhosydd-tools
Section: misc
Architecture: any
Depends:
 rhosydd,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Sensors and actuators service - tools
 Rhosydd mediates access to vehicle sensors and actuators.
 .
 This package contains development and debugging tools.
