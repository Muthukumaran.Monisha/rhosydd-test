/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <locale.h>
#include <string.h>

#include "librhosydd/zone.h"


/* Test that invalid tags are rejected and valid ones are accepted */
static void
test_zone_tag_validation (void)
{
  const gchar *valid_tags[] = {
    "tag",
    "Tag",
    "0",
    "left",
    "right",
  };
  const gchar *invalid_tags[] = {
    NULL,
    "",
    " ",
    "tag!",
    "left,right",
    "top/bottom",
    "‽",
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (valid_tags); i++)
    {
      g_test_message ("Valid tag %" G_GSIZE_FORMAT ": %s", i, valid_tags[i]);
      g_assert_true (rsd_zone_tag_is_valid (valid_tags[i]));
    }

  for (i = 0; i < G_N_ELEMENTS (invalid_tags); i++)
    {
      g_test_message ("Invalid tag %" G_GSIZE_FORMAT ": %s", i,
                      invalid_tags[i]);
      g_assert_false (rsd_zone_tag_is_valid (invalid_tags[i]));
    }
}

/* Test that arrays of tags are valid or invalid as appropriate. */
static void
test_zone_tags_validation (void)
{
  const gchar *empty_tags[] = { NULL };
  const gchar *valid_tags[] = {
    "front",
    "left",
    NULL,  /* terminator */
  };
  const gchar *invalid_tags[] = {
    "left",
    "front",
    "invalid!",
    NULL,  /* terminator */
  };
  const gchar *invalid_order_tags[] = {
    "left",
    "front",
    NULL,  /* terminator */
  };

  g_assert_true (rsd_zone_tags_are_valid (NULL));  /* empty tag list */
  g_assert_true (rsd_zone_tags_are_valid (empty_tags));
  g_assert_true (rsd_zone_tags_are_valid (valid_tags));
  g_assert_false (rsd_zone_tags_are_valid (invalid_tags));
  g_assert_false (rsd_zone_tags_are_valid (invalid_order_tags));
}

/* Test that invalid paths are rejected and valid ones are accepted */
static void
test_zone_path_validation (void)
{
  const gchar *valid_paths[] = {
    "/",
    "//",
    "///",
    "/left/",
    "/left/right//",
    "/left,right//bottom,top/",
  };
  const gchar *invalid_paths[] = {
    NULL,
    "",
    " ",
    "/invalid-tag!/",
    "/left",  /* doesn’t end with a slash */
    "left/",  /* doesn’t start with a slash */
    "‽",
    "/,/",
    "/,,/",
    "/left,/",
    "/,left/",
    "/left,right//top,bottom/",  /* invalid tag ordering */
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (valid_paths); i++)
    {
      g_test_message ("Valid path %" G_GSIZE_FORMAT ": %s", i, valid_paths[i]);
      g_assert_true (rsd_zone_path_is_valid (valid_paths[i]));
    }

  for (i = 0; i < G_N_ELEMENTS (invalid_paths); i++)
    {
      g_test_message ("Invalid path %" G_GSIZE_FORMAT ": %s", i,
                      invalid_paths[i]);
      g_assert_false (rsd_zone_path_is_valid (invalid_paths[i]));
    }
}

static void
assert_is_interned (const gchar *str)
{
  g_assert (str == g_intern_string (str));
}

static const gchar *_tags_left[] = { "left", NULL };
static const gchar *_tags_right[] = { "right", NULL };
static const gchar *_tags_bottom_top[] = { "bottom", "top", NULL };

static const struct {
  const gchar *path;
  const gchar *parent_path;
  const gchar * const *tags;
} path_build_tail_vectors[] = {
  { "/", "", NULL },
  { "/left/", "/", _tags_left },
  { "/left//", "/left/", NULL },
  { "/left///", "/left//", NULL },
  { "/left//right/", "/left//", _tags_right },
  { "//bottom,top/", "//", _tags_bottom_top },
};

/* Test that building paths works correctly. */
static void
test_zone_path_build (void)
{
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (path_build_tail_vectors); i++)
    {
      const gchar *out;

      g_test_message ("Vector %" G_GSIZE_FORMAT ": %s", i,
                      path_build_tail_vectors[i].path);

      out = rsd_zone_path_build (path_build_tail_vectors[i].parent_path,
                                 path_build_tail_vectors[i].tags);

      g_assert_cmpstr (out, ==, path_build_tail_vectors[i].path);
      assert_is_interned (out);
    }
}

static void
assert_cmp_tags (const gchar * const *tags1,
                 const gchar * const *tags2)
{
  gsize i;

  /* Compare the tags, which must be in the expected order. */
  g_assert ((tags1 == NULL) == (tags2 == NULL));

  for (i = 0;
       tags1 != NULL && tags2 != NULL &&
       tags1[i] != NULL && tags2[i] != NULL;
       i++)
    {
      g_assert_cmpstr (tags1[i], ==, tags2[i]);
      assert_is_interned (tags2[i]);
    }
}

/* Test that splitting the tail off paths works correctly. */
static void
test_zone_path_tail (void)
{
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (path_build_tail_vectors); i++)
    {
      g_autofree const gchar **tags = NULL;
      const gchar *parent_path;

      g_test_message ("Vector %" G_GSIZE_FORMAT ": %s", i,
                      path_build_tail_vectors[i].path);

      tags = rsd_zone_path_tail (path_build_tail_vectors[i].path, &parent_path);

      g_assert_cmpstr (parent_path, ==, path_build_tail_vectors[i].parent_path);
      assert_is_interned (parent_path);

      assert_cmp_tags (path_build_tail_vectors[i].tags, tags);
    }
}

/* Test that iterating over a valid zone path works. */
static void
test_zone_path_iter (void)
{
  typedef struct {
    const gchar *parent_path;
    const gchar * const *tags;
  } IterStage;

  const IterStage stages_root[] = {
    { "", NULL },
  };
  const IterStage stages_left[] = {
    { "", NULL },
    { "/", _tags_left },
  };
  const IterStage stages_complex[] = {
    { "", NULL },
    { "/", _tags_left },
    { "/left/", NULL },
    { "/left//", _tags_bottom_top },
    { "/left//bottom,top/", NULL },
  };

  const struct {
    const gchar *path;
    gsize n_stages;
    const IterStage *stages;
  } vectors[] = {
    { "/", G_N_ELEMENTS (stages_root), stages_root },
    { "/left/", G_N_ELEMENTS (stages_left), stages_left },
    { "/left//bottom,top//", G_N_ELEMENTS (stages_complex), stages_complex },
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      RsdZonePathIter iter;
      const gchar *parent_path;
      g_autofree const gchar **tags = NULL;
      gsize j;

      g_test_message ("Vector %" G_GSIZE_FORMAT, i);

      rsd_zone_path_iter_init (&iter, vectors[i].path);

      for (j = 0; j < vectors[i].n_stages; j++)
        {
          g_assert_true (rsd_zone_path_iter_next (&iter, &parent_path, &tags));

          g_assert_cmpstr (parent_path, ==, vectors[i].stages[j].parent_path);
          assert_is_interned (parent_path);

          assert_cmp_tags (vectors[i].stages[j].tags, tags);

          g_clear_pointer (&tags, g_free);
        }

      g_assert_false (rsd_zone_path_iter_next (&iter, &parent_path, &tags));
    }
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/zone/tag/validation", test_zone_tag_validation);
  g_test_add_func ("/zone/tags/validation", test_zone_tags_validation);
  g_test_add_func ("/zone/path/validation", test_zone_path_validation);
  g_test_add_func ("/zone/path/build", test_zone_path_build);
  g_test_add_func ("/zone/path/tail", test_zone_path_tail);
  g_test_add_func ("/zone/path/iter", test_zone_path_iter);

  return g_test_run ();
}
